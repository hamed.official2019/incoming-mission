package ir.hamed.dailymission;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class FragmentMissionList extends Fragment {

    // Constants which indicate type of mission:
    public static final int DAILY_MISSION = 11;
    public static final int WEEKLY_MISSION = 22;
    public static final int MONTHLY_MISSION = 33;
    // Constant which indicates no change in type while updating a row's data in Database:
    public static final int NO_TYPE_CHANGE = 0;

    // Database required fields - an instantiate of MissionDbHelper extended subclass: 'UtilityDatabase'
    UtilityDatabase utilityDatabase;

    // Constants which indicate activation of missions (ignored - true/activated - false)
    public static final boolean ACTIVATED = false; //Unchecked
    public static final boolean IGNORED = true;    //Checked

    // Fields associated to appearance - layouts' views
    private SearchView searchView;
    private RecyclerView dailyScoreRecycler, weeklyScoreRecycler, monthlyScoreRecycler;
    private AppCompatTextView titleDaily, titleWeekly, titleMonthly;
    private View firstDivider, secondDivider, spacerView;

    // Fields handling object populate, show, remove, & reuse(reshow) based on mission's data classes
    private List<DataMission> dailyMissionList;
    private DataMissionAdapter dailyMissionAdapter;

    private List<DataMission> weeklyMissionList;
    private DataMissionAdapter weeklyMissionAdapter;

    private List<DataMission> monthlyMissionList;
    private DataMissionAdapter monthlyMissionAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        utilityDatabase = new UtilityDatabase(getActivity());
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = getLayoutInflater().inflate(R.layout.fragment_mission_list, container, false);
        // Initializing views
        searchView = rootView.findViewById(R.id.search_bar);
        // Recycler Views of each table data explorer:
        dailyScoreRecycler = rootView.findViewById(R.id.daily_score_recycler);
        weeklyScoreRecycler = rootView.findViewById(R.id.weekly_score_recycler);
        monthlyScoreRecycler = rootView.findViewById(R.id.monthly_score_recycler);
        // Titles of each mission definer:
        titleDaily = rootView.findViewById(R.id.daily_mission_title);
        titleWeekly = rootView.findViewById(R.id.weekly_mission_title);
        titleMonthly = rootView.findViewById(R.id.monthly_mission_title);
        // Dividers & spacer:
        firstDivider = rootView.findViewById(R.id.first_divider);
        secondDivider = rootView.findViewById(R.id.second_divider);
        spacerView = rootView.findViewById(R.id.spacer_view);

        // Generic mission recycler methods providing recyclers adapter & list, & transferring data from it's base(SQLite)
        prepareMissionData();
        showMissionData();

        // Prepares fragment layout for search results.
        searchView.setOnQueryTextFocusChangeListener((view, isFocused) -> {
            if (isFocused) {
                // Sets spacer Invisible to create free space & gap for SearchBar & the below title while
                // spacer's visibility is gone by default
                spacerView.setVisibility(View.INVISIBLE);
                // Sets all List Fragment views Invisible beside SearchBar.
                setViewsInvisible(true);
            }
        });
        // Searches Database for requested rows & populate them in RecyclerViews.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Assigns to each List an ArrayList.
                assignArrayList();
                // Searches requested row in database tables.
                dailyMissionList = utilityDatabase.readMissionRow(query, UtilityDatabase.TB_DAILY);
                weeklyMissionList = utilityDatabase.readMissionRow(query, UtilityDatabase.TB_WEEKLY);
                monthlyMissionList = utilityDatabase.readMissionRow(query, UtilityDatabase.TB_MONTHLY);

                // Sets views expressing daily missions visible.
                if (!dailyMissionList.isEmpty()) {
                    dailyScoreRecycler.setVisibility(View.VISIBLE);
                    titleDaily.setVisibility(View.VISIBLE);
                }

                // Sets views expressing weekly missions visible.
                if (!weeklyMissionList.isEmpty()) {
                    weeklyScoreRecycler.setVisibility(View.VISIBLE);
                    titleWeekly.setVisibility(View.VISIBLE);
                    if (!dailyMissionList.isEmpty())
                        firstDivider.setVisibility(View.VISIBLE);
                }

                // Sets views expressing monthly missions visible.
                if (!monthlyMissionList.isEmpty()) {
                    monthlyScoreRecycler.setVisibility(View.VISIBLE);
                    titleMonthly.setVisibility(View.VISIBLE);
                    if (!weeklyMissionList.isEmpty())
                        secondDivider.setVisibility(View.VISIBLE);
                }

                // Populates matched rows of SQLite database to Query in RecyclerViews.
                showMissionData();

                // Tests all tables storage. Toasts no result found.
                if (dailyMissionList.isEmpty() && weeklyMissionList.isEmpty() && monthlyMissionList.isEmpty())
                    Toast.makeText(getActivity(), "Mission not found!", Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Note: Developer.Android provided guide:
                //       true if the query has been handled by the listener,
                //       false to let the SearchView perform the default action.
                return false;
            }
        });
        // Repopulates all rows of SQLite database in RecyclerViews.
        searchView.setOnCloseListener(() -> {
            // Sets spacer's visibility gone to fix default alignment.
            spacerView.setVisibility(View.GONE);
            // Sets all List Fragment views Visible.
            setViewsInvisible(false);

            // Populates all rows of SQLite database in RecyclerViews.
            prepareMissionData();
            showMissionData();
            return false;
        });
        return rootView;
    }

    // Connects Adapter to Recycler & populates mission rows.
    private void showMissionData() {
        // Daily adapter init. & associated to recycler
        dailyMissionAdapter = new DataMissionAdapter(dailyMissionList);
        dailyScoreRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        dailyScoreRecycler.setAdapter(dailyMissionAdapter);

        // Weekly adapter init. & associated to recycler
        weeklyMissionAdapter = new DataMissionAdapter(weeklyMissionList);
        weeklyScoreRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        weeklyScoreRecycler.setAdapter(weeklyMissionAdapter);

        // Monthly adapter init. & associated to recycler
        monthlyMissionAdapter = new DataMissionAdapter(monthlyMissionList);
        monthlyScoreRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        monthlyScoreRecycler.setAdapter(monthlyMissionAdapter);
    }

    // Initializes missions' List from SQLite database.
    private void prepareMissionData() {
        // Avoids null pointer exception, assigns to each List an ArrayList.
        assignArrayList();

        // Populates RecyclerViews' rows.
        dailyMissionList = utilityDatabase.readEntireTableRows(UtilityDatabase.TB_DAILY);
        // Sets visibility of RecyclerView, title, & divider GONE if there's no row to show.
        if (dailyMissionList.isEmpty()) {
            dailyScoreRecycler.setVisibility(View.GONE);
            titleDaily.setVisibility(View.GONE);
            firstDivider.setVisibility(View.GONE);
        }
        weeklyMissionList = utilityDatabase.readEntireTableRows(UtilityDatabase.TB_WEEKLY);
        // Sets visibility of RecyclerView, title, & divider GONE if there's no row to show.
        if (weeklyMissionList.isEmpty()) {
            weeklyScoreRecycler.setVisibility(View.GONE);
            titleWeekly.setVisibility(View.GONE);
            secondDivider.setVisibility(View.GONE);
        }
        monthlyMissionList = utilityDatabase.readEntireTableRows(UtilityDatabase.TB_MONTHLY);
        // Sets visibility of RecyclerView & title GONE if there's no row to show.
        if (monthlyMissionList.isEmpty()) {
            monthlyScoreRecycler.setVisibility(View.GONE);
            titleMonthly.setVisibility(View.GONE);
        }
    }

    // Utility Methods: Assigns to each List a new ArrayList or clear data of ex-created.
    private void assignArrayList() {
        if (dailyMissionList == null)
            dailyMissionList = new ArrayList<>();
        else
            dailyMissionList.clear();

        // Avoids null pointer exception
        if (weeklyMissionList == null)
            weeklyMissionList = new ArrayList<>();
        else
            weeklyMissionList.clear();

        // Avoids null pointer exception
        if (monthlyMissionList == null)
            monthlyMissionList = new ArrayList<>();
        else
            monthlyMissionList.clear();
    }

    // Utility Methods: Sets visibilities based on searchBar focus result.
    private void setViewsInvisible(Boolean isFocused) {
        // Defines & assigns the visibility views.
        if (isFocused) {
            // Recyclers:
            dailyScoreRecycler.setVisibility(View.GONE);
            weeklyScoreRecycler.setVisibility(View.GONE);
            monthlyScoreRecycler.setVisibility(View.GONE);
            // Titles:
            titleDaily.setVisibility(View.GONE);
            titleWeekly.setVisibility(View.GONE);
            titleMonthly.setVisibility(View.GONE);
            // Dividers:
            firstDivider.setVisibility(View.GONE);
            secondDivider.setVisibility(View.GONE);
        } else {
            // Recyclers:
            dailyScoreRecycler.setVisibility(View.VISIBLE);
            weeklyScoreRecycler.setVisibility(View.VISIBLE);
            monthlyScoreRecycler.setVisibility(View.VISIBLE);
            // Titles:
            titleDaily.setVisibility(View.VISIBLE);
            titleWeekly.setVisibility(View.VISIBLE);
            titleMonthly.setVisibility(View.VISIBLE);
            // Dividers:
            firstDivider.setVisibility(View.VISIBLE);
            secondDivider.setVisibility(View.VISIBLE);
        }
    }
}
