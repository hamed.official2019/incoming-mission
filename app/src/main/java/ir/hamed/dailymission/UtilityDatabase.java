package ir.hamed.dailymission;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class UtilityDatabase {
    // Constant key-names for tables of mission database:
    public static final String TB_DAILY = "daily_mission_tb";
    public static final String TB_WEEKLY = "weekly_mission_tb";
    public static final String TB_MONTHLY = "monthly_mission_tb";

    // Gives access to database. Read/Write type defined in each method.
    private MissionDbHelper dbHelper;
    private SQLiteDatabase liteDatabase;
    // Holds cursor reading data of database:
    private Cursor cursor;
    // A List to receive & restore as many pointed missions:
    private List<DataMission> missionList;

    // Holds a context to access to methods which request Context parameters
    private Context context;

    // Defines to show delete Toast or not. In case of updating a mission delete Toast is not required.
    private boolean showDeleteToast;

    public UtilityDatabase(@Nullable Context context) {
        this.dbHelper = new MissionDbHelper(context);
        this.context = context;
        // Permits delete Toast to be shown by default in result of delete method execution.
        showDeleteToast = true;
    }

    // Adds new rows in database.                                                        --Note: Create Function of CRUD Operations--
    public void insertMissionRow(int type, String name, double score, String description, boolean ignored) {
        // Database instantiate initialized writable
        liteDatabase = dbHelper.getWritableDatabase();

        // A new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        if (isTypeDefined(type))
            values.put(DataMission.KEY_TYPE, type);
        else
            return;
        if (isNameUnique(name))
            values.put(DataMission.KEY_NAME, name);
        else {
            Toast.makeText(context, "Name already used!", Toast.LENGTH_SHORT).show();
            return;
        }
        values.put(DataMission.KEY_SCORE, score);
        if (description == null) {
            values.putNull(DataMission.KEY_DESCRP);
            Toast.makeText(context, "No description is wrote", Toast.LENGTH_SHORT).show();
        } else {
            values.put(DataMission.KEY_DESCRP, description);
        }
        values.put(DataMission.KEY_IGNORE, ignored);

        // Inserts the new row, returns the primary key value of the new row
        long newRowId;
        switch (type) {
            case FragmentMissionList.DAILY_MISSION:
                newRowId = liteDatabase.insert(TB_DAILY, description, values);
                break;
            case FragmentMissionList.WEEKLY_MISSION:
                newRowId = liteDatabase.insert(TB_WEEKLY, description, values);
                break;
            case FragmentMissionList.MONTHLY_MISSION:
                newRowId = liteDatabase.insert(TB_MONTHLY, description, values);
                break;
        }

        // Closes opened database
        if (liteDatabase.isOpen())
            liteDatabase.close();
    }

    //                                                                                  --Note: Read Function of CRUD Operations--
    // Reads whole database & searches for the entered keyword even partial letters used in name or description fields.
    public List<DataMission> readMissionRow(String keyWord, String tableType) {
        liteDatabase = dbHelper.getReadableDatabase();
        // Holds all rows including entered keyword in name or description.
        missionList = new ArrayList<>();

        // Filters results WHERE columnName = 'KEY_COLUMN_NAME'.
        String selection = DataMission.KEY_NAME + " LIKE ?";
        String[] selectionArgs = {"%" + keyWord + "%"};

        // Reads requested table's mission(s) matching to request (searched keyword).
        cursor = liteDatabase.query(tableType, null, selection,
                selectionArgs, null, null, DataMission.KEY_NAME);
        // Reads any cursor's row if existed
        if (cursor.moveToFirst()) {
            do {
                populateMission();
            } while (cursor.moveToNext());
        }

        // Closes opened database & cursor
        if (!cursor.isClosed())
            cursor.close();
        if (liteDatabase.isOpen())
            liteDatabase.close();
        // Returns all reached missions in tables & then stored in the List.
        return missionList;
    }

    // Only reads the mentioned table & initializes an ArrayList.                        --Note: Read Function of CRUD Operations--
    public List<DataMission> readEntireTableRows(String tableType) {
        liteDatabase = dbHelper.getReadableDatabase();
        // Holds all rows including entered keyword in name or description.
        missionList = new ArrayList<>();

        // Reads all table's existed mission(s).
        cursor = liteDatabase.query(tableType, null, null, null, null, null, null);
        // Note: cursor declared of private field, not initializing it -else condition- prevents running next if condition body.

        // Reads any cursor's row if existed
        if (cursor.moveToFirst()) {
            do {
                populateMission();
            } while (cursor.moveToNext());
        }

        // Closes opened database & cursor
        if (!cursor.isClosed())
            cursor.close();
        if (liteDatabase.isOpen())
            liteDatabase.close();
        // Returns all reached missions in table & then stored in the List.
        return missionList;
    }

    // Looks for a specified row & updates its columns                                   --Note: Update Function of CRUD Operations--
    // NOTE: A 'null' -key- value provides exception column(s), exceptions keep their current value!
    // NOTE: Primitive double doesn't receive null values, a '0.00' -key- value provides exception score column.
    // NOTE: Primitive boolean doesn't receive null values, argument set as a String which can receive: {null, true, false}.
    @SuppressLint("Range")
    public boolean updateMissionRow(String nameCol, int newType, String newName, double newScore, String newDescription, String newIgnoreString) {
        // Database instantiate initialized writable
        liteDatabase = dbHelper.getWritableDatabase();

        // Integers saving returning results of each table -search & - update method. Results: {0-not found, 1-a row updated}
        int isRowInDailyTb = -10, isRowInWeeklyTb = -10, isRowInMonthlyTb = -10;
        // WHERE clause dedicating which row to be affected (updated)
        String whereClause = DataMission.KEY_NAME + " LIKE ?";
        String[] whereValue = {nameCol};

        // Receives, analyses, & alters columns of requested row
        ContentValues contentValues = new ContentValues();
        // Local variables holding old fields of the row:
        String oldName, oldDescription;
        int oldType = 0;
        double oldScore;
        boolean isTypeChanged = false, oldIgnore;

        // Initializes a cursor, uses name column to search -          IMPORTING DATA TO CONTENT VALUE
        cursor = readExactMissionRow(nameCol);
        // Checks nullability of entered keyword nameColumn (mentioned row)
        if (cursor != null && cursor.getString(cursor.getColumnIndex(DataMission.KEY_NAME)).equals(nameCol) && cursor.getCount() == 1) {
            // Checks inputted values of type parameter to be as defined Constants.
            if (isTypeDefined(newType)) {
                oldType = cursor.getInt(cursor.getColumnIndex(DataMission.KEY_TYPE));
                // Checks if mission's type has changed & if yes to delete existed row then to create a new one in requested type table
                if (newType == FragmentMissionList.NO_TYPE_CHANGE) {
                    // Defines the table which alters the old row.
                    isTypeChanged = false;
                    // Assigns value 'type' to altered row
                    contentValues.put(DataMission.KEY_TYPE, oldType);
                } else {
                    // Defines the table in which old row must be deleted & the table in which new row must be added.
                    isTypeChanged = true;
                    // Assigns value 'type' to created row
                    contentValues.put(DataMission.KEY_TYPE, newType);
                }
            }

            // Mission's name:
            if (newName == null) {
                // Keeps mission's old name
                oldName = cursor.getString(cursor.getColumnIndex(DataMission.KEY_NAME));
                contentValues.put(DataMission.KEY_NAME, oldName);
            } else
                // Alters mission's name
                contentValues.put(DataMission.KEY_NAME, newName);

            // Mission's score:
            if (newScore == 0.00) {
                // Keeps mission's old score
                oldScore = cursor.getDouble(cursor.getColumnIndex(DataMission.KEY_SCORE));
                contentValues.put(DataMission.KEY_SCORE, oldScore);
            } else
                // Alters mission's score
                contentValues.put(DataMission.KEY_SCORE, newScore);

            // Mission's description:
            if (newDescription == null) {
                // Keeps mission's old description
                oldDescription = cursor.getString(cursor.getColumnIndex(DataMission.KEY_DESCRP));
                contentValues.put(DataMission.KEY_DESCRP, oldDescription);
            } else
                // Alters mission's description
                contentValues.put(DataMission.KEY_DESCRP, newDescription);

            // Mission's ignore:
            if (newIgnoreString == null) {
                // Keeps mission's old ignore
                int tempBoolIndicator;
                // Receives int as sqlite stores boolean to 0-false, 1-true INTEGER; then short hand If returns boolean value.
                tempBoolIndicator = cursor.getInt(cursor.getColumnIndex(DataMission.KEY_IGNORE));
                oldIgnore = tempBoolIndicator == 1;
                contentValues.put(DataMission.KEY_IGNORE, oldIgnore);
            } else {
                // Alters mission's ignore:
                boolean newIgnore = Boolean.parseBoolean(newIgnoreString.toLowerCase());
                contentValues.put(DataMission.KEY_IGNORE, newIgnore);
            }
        } else {
            Toast.makeText(context, "Failed to change!", Toast.LENGTH_SHORT).show();
            // Closes called cursor
            if (!cursor.isClosed())
                cursor.close();
            // Closes opened database
            if (liteDatabase.isOpen())
                liteDatabase.close();
            return false;
        }
        // Closes called cursor
        if (!cursor.isClosed())
            cursor.close();

        // Checks the newName to be unique. If any row with the same name existed ends operation & prevents exception crash.
        if (newName != null)
            if (!isNameUnique(newName)) {
                Toast.makeText(context, "Update failed.\nName is already used!", Toast.LENGTH_SHORT).show();
                // Closes called cursor
                if (!cursor.isClosed())
                    cursor.close();
                // Closes opened database
                if (liteDatabase.isOpen())
                    liteDatabase.close();
                return false;
            }

        // Prevents delete Toast to be Shown while delete method is took by update process.
        showDeleteToast = false;

        // Decides to alter changes in existed row, or to create a new one & to delete the old one.
        if (isTypeChanged) {                 //Note: In case that mission's type changed. Have to delete the old one & create a new one.

            // Deletes existed row
            deleteMissionRow(nameCol);
            // Re-opens closed database object in upper delete crud operation
            if (!liteDatabase.isOpen())
                liteDatabase = dbHelper.getWritableDatabase();
            // Inserts the new row. Note: Method returns the primary key value of the new row which is not used. Its useful for try-catch!!!
//          long newRowId;
            switch (newType) {
                case FragmentMissionList.DAILY_MISSION:
                    // Alters changes in existed row in daily table
                    liteDatabase.insert(TB_DAILY, newDescription, contentValues);
                    Toast.makeText(context,
                            "mission " + nameCol + " details changed!", Toast.LENGTH_SHORT).show();
                    break;
                case FragmentMissionList.WEEKLY_MISSION:
                    // Alters changes in existed row in weekly table
                    liteDatabase.insert(TB_WEEKLY, newDescription, contentValues);
                    Toast.makeText(context,
                            "mission " + nameCol + " details changed!", Toast.LENGTH_SHORT).show();
                    break;
                case FragmentMissionList.MONTHLY_MISSION:
                    // Alters changes in existed row in monthly table
                    liteDatabase.insert(TB_MONTHLY, newDescription, contentValues);
                    Toast.makeText(context,
                            "mission " + nameCol + " details changed!", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {                   // Note: In case that mission's type IS NOT changed. Keeps the old one, just alters changes.

            // Defines which table should alter.
            // Note: newType is null, & oldType contains case definer value
            switch (oldType) {
                case FragmentMissionList.DAILY_MISSION:
                    // Searches daily table & updates requested row if existed; if any row updated breaks.
                    isRowInDailyTb = liteDatabase.update(TB_DAILY, contentValues, whereClause, whereValue);
                    if (isRowInDailyTb > 0) {
                        Toast.makeText(context,
                                "mission " + nameCol + " details changed!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case FragmentMissionList.WEEKLY_MISSION:
                    // Searches weekly table & updates requested row if existed; if any row updated breaks.
                    isRowInWeeklyTb = liteDatabase.update(TB_WEEKLY, contentValues, whereClause, whereValue);
                    if (isRowInWeeklyTb > 0) {
                        Toast.makeText(context,
                                "mission " + nameCol + " details changed!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case FragmentMissionList.MONTHLY_MISSION:
                    // Searches monthly table & updates requested row if existed; if any row updated breaks.
                    isRowInMonthlyTb = liteDatabase.update(TB_MONTHLY, contentValues, whereClause, whereValue);
                    if (isRowInMonthlyTb > 0) {
                        Toast.makeText(context,
                                "mission " + nameCol + " details changed!", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }

        // Closes opened database
        if (liteDatabase.isOpen())
            liteDatabase.close();
        // Results update operation succeed.
        return true;
    }

    // Deletes selected row. This method is USED in updateMissionRow method!!             --Note: Delete Function of CRUD Operations--
    public void deleteMissionRow(String nameCol) {
        // Database instantiate initialized writable
        liteDatabase = dbHelper.getWritableDatabase();

        // Integers saving returning results of each table -search & - delete method. Results: {0-not found, 1-a row deleted}
        int isRowInDailyTb, isRowInWeeklyTb, isRowInMonthlyTb;
        // WHERE clause to apply when deleting
        String whereClause = DataMission.KEY_NAME + " LIKE ?";
        String[] whereValue = {nameCol};

        // Searches daily table & deletes requested row if existed; if any row deleted breaks.
        isRowInDailyTb = liteDatabase.delete(TB_DAILY, whereClause, whereValue);
        if (isRowInDailyTb > 0) {
            if (showDeleteToast)
                Toast.makeText(context,
                        "mission " + nameCol + " successfully deleted!", Toast.LENGTH_SHORT).show();

            // Closes opened database
            if (liteDatabase.isOpen())
                liteDatabase.close();
            return;
        }

        // Searches weekly table & deletes requested row if existed; if any row deleted breaks.
        isRowInWeeklyTb = liteDatabase.delete(TB_WEEKLY, whereClause, whereValue);
        if (isRowInWeeklyTb > 0) {
            if (showDeleteToast)
                Toast.makeText(context,
                        "mission " + nameCol + " successfully deleted!", Toast.LENGTH_SHORT).show();

            // Closes opened database
            if (liteDatabase.isOpen())
                liteDatabase.close();
            return;
        }

        // Searches monthly table & deletes requested row if existed; if any row deleted breaks.
        isRowInMonthlyTb = liteDatabase.delete(TB_MONTHLY, whereClause, whereValue);
        if (isRowInMonthlyTb > 0) {
            if (showDeleteToast)
                Toast.makeText(context,
                        "mission " + nameCol + " successfully deleted!", Toast.LENGTH_SHORT).show();
        }

        // Closes opened database
        if (liteDatabase.isOpen())
            liteDatabase.close();
    }

    // NOTE: This is a utility method & it's not used directly in any fragment or any activity
    // Reads a mission whose name equals exactly to the entered keyword, if the mission existed.
    private Cursor readExactMissionRow(String keyWord) {
        liteDatabase = dbHelper.getReadableDatabase();

        // Filters results WHERE columnName = 'KEY_COLUMN_NAME'
        String selection = DataMission.KEY_NAME + " LIKE ?";
        String[] selectionArgs = {keyWord};

        // Reads daily table's mission(s)
        cursor = liteDatabase.query(TB_DAILY, null, selection,
                selectionArgs, null, null, DataMission.KEY_NAME);
        // Returns searched row upon the cursor if the row existed
        if (cursor.moveToFirst())
            return cursor;

        // Reads weekly table's mission(s)
        cursor = liteDatabase.query(TB_WEEKLY, null, selection,
                selectionArgs, null, null, DataMission.KEY_NAME);
        // Returns searched row upon the cursor if the row existed
        if (cursor.moveToFirst())
            return cursor;

        // Reads monthly table's mission(s)
        cursor = liteDatabase.query(TB_MONTHLY, null, selection,
                selectionArgs, null, null, DataMission.KEY_NAME);
        // Returns searched row upon the cursor if the row existed
        if (cursor.moveToFirst())
            return cursor;

        // Cursor haven't been close, remember to close wherever its used.
        // Returns null if requested row isn't existed.
        return null;
    }

    // NOTE: This is a utility method & it's not used directly in any fragment or any activity
    // Searches in whole database field name, indicates usage of inserted mission's name.
    // Returns 'true' if name is unique & is ready to use.  Returns 'false' if name is used.
    private boolean isNameUnique(String name) {
        // An instantiate of customized database open helper & SQLite database
        liteDatabase = dbHelper.getReadableDatabase();

        // Declares a Query to read each table's name column values
        final String CMD_READ_NAME_DAILY = "SELECT " + DataMission.KEY_NAME + " FROM " + TB_DAILY + ";";
        final String CMD_READ_NAME_WEEKLY = "SELECT " + DataMission.KEY_NAME + " FROM " + TB_WEEKLY + ";";
        final String CMD_READ_NAME_MONTHLY = "SELECT " + DataMission.KEY_NAME + " FROM " + TB_MONTHLY + ";";

        // Searches daily table's name column
        cursor = liteDatabase.rawQuery(CMD_READ_NAME_DAILY, null);
        if (cursor.moveToFirst()) {
            do {
                @SuppressLint("Range") String isItEqual = cursor.
                        getString(cursor.getColumnIndex(DataMission.KEY_NAME)).toLowerCase();
                if (isItEqual.equals(name.toLowerCase())) {
                    cursor.close();
                    return false;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();


        // Searches weekly table's name column
        cursor = liteDatabase.rawQuery(CMD_READ_NAME_WEEKLY, null);
        if (cursor.moveToFirst()) {
            do {
                @SuppressLint("Range") String isItEqual = cursor.
                        getString(cursor.getColumnIndex(DataMission.KEY_NAME)).toLowerCase();
                if (isItEqual.equals(name.toLowerCase())) {
                    cursor.close();
                    return false;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();


        // Searches monthly table's name column
        cursor = liteDatabase.rawQuery(CMD_READ_NAME_MONTHLY, null);
        if (cursor.moveToFirst()) {
            do {
                @SuppressLint("Range") String isItEqual = cursor.
                        getString(cursor.getColumnIndex(DataMission.KEY_NAME)).toLowerCase();
                if (isItEqual.equals(name.toLowerCase())) {
                    cursor.close();
                    return false;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return true;
    }

    // NOTE: This is a utility method & it's not used directly in any fragment or any activity
    // Creates new mission Object, then assigns values to its fields, and finally added to mission List
    @SuppressLint("Range")
    private void populateMission() {
        // New mission object receiving values:
        DataMission mission = new DataMission();

        // Assigns new missions' values
        mission.setType(cursor.getInt(cursor.getColumnIndex(DataMission.KEY_TYPE)));
        mission.setName(cursor.getString(cursor.getColumnIndex(DataMission.KEY_NAME)));
        mission.setScore(cursor.getDouble(cursor.getColumnIndex(DataMission.KEY_SCORE)));
        // Receives int as sqlite stores boolean to 0-false, 1-true INTEGER; then short hand If returns boolean value.
        int booleanIndicator = cursor.getInt(cursor.getColumnIndex(DataMission.KEY_IGNORE));
        // (booleanIndicator == 0)
        mission.setIgnored(booleanIndicator == 1);
        mission.setDescription(cursor.getString(cursor.getColumnIndex(DataMission.KEY_DESCRP)));

        // Adds new mission to the mission List
        missionList.add(mission);
    }

    // NOTE: This is a utility method & it's not used directly in any fragment or any activity
    // Checks the inserted value as a parameter "int type" to be of a primarily defined Constants.
    private boolean isTypeDefined(int type) {
        // Returns true if the parameter type is equal to any of defined mission type keywords.
        return type == FragmentMissionList.DAILY_MISSION || type == FragmentMissionList.WEEKLY_MISSION
                || type == FragmentMissionList.MONTHLY_MISSION || type == FragmentMissionList.NO_TYPE_CHANGE;
    }
}
