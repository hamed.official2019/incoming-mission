package ir.hamed.dailymission;

import static ir.hamed.dailymission.MainActivity.fragManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

public class FragmentMainScores extends Fragment implements Animation.AnimationListener {
    // Views including buttons & scores.
    private AppCompatImageButton shareButton, newMissionButton, listPreviewButton;
    private AppCompatTextView totalScoreTextView, prizeScoreTextView, shareMeTextView;
    private AppCompatTextView dailyScoreTextView, weeklyScoreTextView, monthlyScoreTextView;

    // Gives access to Date SharedPreference Data, assigned in MainActivity.
    private SharedPreferences datePrefPrior;

    private boolean isShareMeAnimeRun = false;

    // Variables to calculate coordinates using utility class "UtilityScreen"
    float pxHeight, pxWidth;
    float dpHeight, dpWidth;
    float density;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = getLayoutInflater().inflate(R.layout.fragment_main_scores, container, false);

        // Button Views:
        shareButton = rootView.findViewById(R.id.share_button);
        newMissionButton = rootView.findViewById(R.id.new_mission_button);
        listPreviewButton = rootView.findViewById(R.id.list_preview_btn);
        shareMeTextView = rootView.findViewById(R.id.share_me_text_view);
        // Note: no access verified to title text views. No declaration, no initialization.
        // Score presenter text Views (which their context is accessible & will be updated):
        totalScoreTextView = rootView.findViewById(R.id.total_score_text_view);
        prizeScoreTextView = rootView.findViewById(R.id.prize_score_text_view);
        dailyScoreTextView = rootView.findViewById(R.id.daily_score_text_view);
        weeklyScoreTextView = rootView.findViewById(R.id.weekly_score_text_view);
        monthlyScoreTextView = rootView.findViewById(R.id.monthly_score_text_view);

        // Initializes SharedPref, reading stored Data of the DatePref created in MainActivity.
        datePrefPrior = rootView.getContext().getSharedPreferences(MainActivity.KEY_PREF_NAME, Context.MODE_PRIVATE);
        // Assigns value to score text views.
        totalScoreTextView.setText(datePrefPrior.getString(MainActivity.KEY_SCORE_TOTAL, "0.00"));
        prizeScoreTextView.setText(datePrefPrior.getString(MainActivity.KEY_SCORE_PRIZE, "0.00"));
        dailyScoreTextView.setText(datePrefPrior.getString(MainActivity.KEY_SCORE_DAY, "0.00"));
        weeklyScoreTextView.setText(datePrefPrior.getString(MainActivity.KEY_SCORE_WEEK, "0.00"));
        monthlyScoreTextView.setText(datePrefPrior.getString(MainActivity.KEY_SCORE_MONTH, "0.00"));

        // Calculating width & height of displaying screen.
        displayMeasurement();

        // Setting anime to share me text once app start
        if (!isShareMeAnimeRun) {
            //Aligns share me text using layout params margins
            alignShareMeTxt();
            Animation shareMeAnime = AnimationUtils.loadAnimation(getActivity(), R.anim.share_icon_anime);
            // Sets visibility of view GONE on end of anime
            shareMeAnime.setAnimationListener(this);
            shareMeTextView.startAnimation(shareMeAnime);
            isShareMeAnimeRun = true;
        } else // Dedicating condition of "share ME!" text as back button pressed(backStack).
            shareMeTextView.setVisibility(View.GONE);

        // Checks display screen size, sets on Click to drop down btn if existed (only in small display screens).
        if (!MainActivity.isTwoPane) { // Result is false - A small display screen device is running the App
            // Redirects to mission listing fragment.
            listPreviewButton.setOnClickListener(this::onDropDownClick);
        }

        // Redirects to creating mission fragment.
        newMissionButton.setOnClickListener(this::saveMission);

        return rootView;
    }

    // Aligning share me text using layout params margins - NOTE: each screen size got a different pattern
    private void alignShareMeTxt() {
        // Calculating margins from each side by default display window measurements - small windows display
        //NOTE: the result is in Pixels, therefore margin should be in px.
        int rightMarginShareTVSmallDisp = (int) (this.pxWidth / 4 + 25);
        int leftMarginShareTVSmallDisp = (int) (this.pxWidth - (rightMarginShareTVSmallDisp + this.pxWidth / 6));

        // Converting dp to pixel for top margin
        int topMarginShareTVSmallDisp = (int) (13 * this.density);

        // Calculating margins from each side by default display window measurements - small windows display
        int leftMarginShareTVWideDisp = (int) (this.pxWidth / 10 + 25);
        int bottomMarginShareTVWideDisp = (int) (this.density);


        // Progress of alignment by margins
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams
                (RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        // Width & height compared to be over 600dp, then two diff alignment
        if (this.dpWidth < 600 && this.dpWidth < 600) {
            // NOTE: this method "setMargin" receives pixels as arguments/
            params.setMargins(leftMarginShareTVSmallDisp, topMarginShareTVSmallDisp, 0, 0);
        } else {
            // NOTE: this method "setMargin" receives pixels as arguments.
            params.setMargins(0, 0, leftMarginShareTVWideDisp, bottomMarginShareTVWideDisp);
        }
        shareMeTextView.setLayoutParams(params);
    }

    // Calculating width & height of displaying screen
    private void displayMeasurement() {
        // Instantiate of class UtilityScreen to get width & height of display screen in dp
        UtilityScreen utilityScreen = new UtilityScreen(requireActivity());

        // Default width of display window
        this.pxWidth = utilityScreen.getPxWidth();
        // Default height of display window
        this.pxHeight = utilityScreen.getPxHeight();

        // initializing density which is used in alignShareMeTxt method
        this.density = utilityScreen.getDensity();

        // In order to assign diff margins to share ME! txt to sw600dp (wide) screens or small screens
        this.dpHeight = utilityScreen.getDpHeight();
        this.dpWidth = utilityScreen.getDpWidth();
    }


    @Override
    public void onAnimationStart(Animation animation) {
        shareMeTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        shareMeTextView.setVisibility(View.GONE);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    // Views mission list fragment layout as its button clicked.
    private void onDropDownClick(View view) {
        fragManager
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.bottom_slide_in_frag,         // enter anime
                        R.anim.fade_out_frag,                // exit anime
                        R.anim.fade_in_frag,                 // popEnter anime
                        R.anim.bottom_slide_out_frag         // popExit anime
                ).replace(R.id.fragment_main_scores_container, MainActivity.fragmentMissionList)
                .setReorderingAllowed(true)
                .addToBackStack(null)
                .commit();
    }

    // Views Create-Update mission fragment layout as newMission button clicked.
    private void saveMission(View view) {
        // Defines which fragment Layout to be replaced by frag Create Mission.
        int currentFragId;
        if (MainActivity.isTwoPane)  // Result is true - A wide display screen device is running the App
            currentFragId = R.id.fragment_mission_container;
        else                         // Result is false - A small display screen device is running the App
            currentFragId = R.id.fragment_main_scores_container;

        // Begins transaction & applies animations.
        fragManager
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.right_slide_in_frag,         // enter anime
                        R.anim.fade_out_frag,               // exit anime
                        R.anim.fade_in_frag,                // popEnter anime
                        R.anim.right_slide_out_frag         // popExit anime
                ).replace(currentFragId, MainActivity.fragmentCreateUpdate)
                .setReorderingAllowed(true)
                .addToBackStack("create")
                .commit();
    }
}
