package ir.hamed.dailymission;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    // Constant key to compare week's begin:
    public static final String KEY_MONDAY = "Mon";
    // String type Constants, keyword to access scores of SharedPref & to dedicate score type.
    public static final String KEY_SCORE_DAY = "daily";
    public static final String KEY_SCORE_WEEK = "weekly";
    public static final String KEY_SCORE_MONTH = "monthly";
    public static final String KEY_SCORE_TOTAL = "total";
    public static final String KEY_SCORE_PRIZE = "prize";

    // Constant Keys to access to Pref stored Date (named "savedDate"):
    public static final String KEY_PREF_NAME = "datePref";
    public static final String KEY_DATE_DAY = "savedDay";
    public static final String KEY_DATE_WEEK = "savedWeek";
    public static final String KEY_DATE_MONTH = "savedMonth";
    public static final String NOT_MENTIONED = "Null";

    //Boolean var automatically checking screen size & assigning corresponding layouts.
    public static boolean isTwoPane = false;

    @SuppressLint("StaticFieldLeak")
    public static FragmentMissionList fragmentMissionList;
    public static FragmentMainScores fragmentMainScores;
    public static FragmentCreateUpdate fragmentCreateUpdate;
    // Frag Manager is used to save frags state in back stack - by clicking on back button it will switch to last frag
    // in order of replacement(reversed).
    public static FragmentManager fragManager;

    // Shared Preference & Editor to access to it. Used for saving Date & Scores.
    private SharedPreferences datePreference;
    private SharedPreferences.Editor datePrefEditor;

    // Double variables storing scores:
    private double totalScore, prizeScore, dailyScore, weeklyScore, monthlyScore;
    // Strings receiving current Date & time:
    private int currentDayDate, currentMonthDate;
    private String weekStateDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Initializes Frags, Gets access.
        fragmentMainScores = new FragmentMainScores();
        fragmentMissionList = new FragmentMissionList();
        fragmentCreateUpdate = new FragmentCreateUpdate();
        fragManager = getSupportFragmentManager();

        // Initializes the PrefFrag & Editor. Gives access to them.
        datePreference = getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        datePrefEditor = datePreference.edit();
        // Initializes common local variables.
        initCommonVariable();
        // Sets periodic scores ZERO in the beginning of each period. And assigns prior Data to Pref once App installed & run.
        accessSharedPref();

        // Checking device screen size & assigning associated frag layout.
        isTwoPane = (findViewById(R.id.fragment_mission_container) != null); //The result will be true.
        if (!isTwoPane) { // Device is using a small screen display
            fragManager.beginTransaction()
                    .replace(R.id.fragment_main_scores_container, fragmentMainScores)
                    .commit();
        }
    }

    // initializes current Date & score variables.
    private void initCommonVariable() {
        // Gets current Date & Time, fills them in separated variables.
        this.currentDayDate = Integer.parseInt(new SimpleDateFormat("dd", Locale.getDefault()).format(new Date()));
        this.weekStateDate = new SimpleDateFormat("EEE", Locale.getDefault()).format(new Date());
        this.currentMonthDate = Integer.parseInt(new SimpleDateFormat("MM", Locale.getDefault()).format(new Date()));

        // Retrieves stored score, saves them in temporary variables.
        this.dailyScore = Double.parseDouble(datePreference.getString(KEY_SCORE_DAY, String.valueOf(dailyScore)));
        this.weeklyScore = Double.parseDouble(datePreference.getString(KEY_SCORE_WEEK, String.valueOf(weeklyScore)));
        this.monthlyScore = Double.parseDouble(datePreference.getString(KEY_SCORE_MONTH, String.valueOf(monthlyScore)));
        this.totalScore = Double.parseDouble(datePreference.getString(KEY_SCORE_TOTAL, String.valueOf(totalScore)));
        // Note: Prize score by default set 100 & stored in SharedPref. Todo: Set access to its value in App's settings.
        this.prizeScore = 100;
        datePrefEditor.putString(KEY_SCORE_PRIZE, String.valueOf(prizeScore));
    }

    // Sets periodic scores ZERO in the beginning of each period. And assigns prior Data to Pref once App installed & run.
    private void accessSharedPref() {
        // Stores passed Date & Time variables to compare periods' state.
        int savedDayDate, savedMonthDate;
        String savedWeekState;
        // Receives saved Date in Pref if existed, if not then values by default.
        savedDayDate = datePreference.getInt("savedDay", 0);
        savedWeekState = datePreference.getString("savedWeek", NOT_MENTIONED);
        savedMonthDate = datePreference.getInt("savedMonth", 0);

        // Note: Sets periodic scores ZERO in the beginning of each period.
        // Sets daily score to zero, in new day's arrival.
        if (savedDayDate != currentDayDate) {
            // Revalues saved Date to prevent freeing (zeroing) score unexpectedly.
            datePrefEditor.putInt(KEY_DATE_DAY, currentDayDate);
            datePrefEditor.putString(KEY_SCORE_DAY, "0.000");
        }

        // Sets weekly score to zero, in new week's arrival.
        if (!savedWeekState.equals(weekStateDate)) {
            // Revalues saved Date to prevent freeing (zeroing) score unexpectedly.
            datePrefEditor.putString(KEY_DATE_WEEK, weekStateDate);
            if (weekStateDate.equals(KEY_MONDAY))
                datePrefEditor.putString(KEY_SCORE_WEEK, "0.000");
        }

        // Sets monthly score to zero, in new month's arrival.
        if (savedMonthDate != currentMonthDate) {
            // Revalues saved Date to prevent freeing (zeroing) score unexpectedly.
            datePrefEditor.putInt(KEY_DATE_MONTH, currentMonthDate);
            datePrefEditor.putString(KEY_SCORE_MONTH, "0.000");
        }

        // Commits changes in SharedPref's variables.
        datePrefEditor.apply();
    }

    // Adds recently done mission's score to its field & total score.
    private void computeScore(double newScore, String missionKey) {
        // Calculates & store new scores in day & total scores.
        if (missionKey.equals(KEY_SCORE_DAY)) {
            this.dailyScore += newScore;
            this.totalScore += newScore;
            datePrefEditor.putString(KEY_SCORE_DAY, String.valueOf(dailyScore));
            datePrefEditor.putString(KEY_SCORE_TOTAL, String.valueOf(totalScore));
        }

        // Calculates & store new scores in week & total scores.
        if (missionKey.equals(KEY_SCORE_WEEK)) {
            this.weeklyScore += newScore;
            this.totalScore += newScore;
            datePrefEditor.putString(KEY_SCORE_WEEK, String.valueOf(weeklyScore));
            datePrefEditor.putString(KEY_SCORE_TOTAL, String.valueOf(totalScore));
        }

        // Calculates & store new scores in month & total scores.
        if (missionKey.equals(KEY_SCORE_MONTH)) {
            this.monthlyScore += newScore;
            this.totalScore += newScore;
            datePrefEditor.putString(KEY_SCORE_MONTH, String.valueOf(monthlyScore));
            datePrefEditor.putString(KEY_SCORE_TOTAL, String.valueOf(totalScore));
        }

        // Commits changes in SharedPref's variables.
        datePrefEditor.apply();
    }

    // Checks the prize score to be reached, prize score is planned the target.
    // Todo: Set alarm or any suitable event to notify target score is approached.
    private boolean isPrizeScoreReached() {
        return this.totalScore % this.prizeScore == 0.0;
    }
}