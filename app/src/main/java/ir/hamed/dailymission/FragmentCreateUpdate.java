package ir.hamed.dailymission;

import static ir.hamed.dailymission.MainActivity.fragManager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import java.util.Objects;

public class FragmentCreateUpdate extends Fragment implements AdapterView.OnItemSelectedListener {

    // Constants which indicate type of mission:
    private static final int DAILY_MISSION = 11;
    private static final int WEEKLY_MISSION = 22;
    private static final int MONTHLY_MISSION = 33;
    // String type Constants - Note: App presents text to user, type stores int in Db.
    private static final String STRING_DAILY_ITEM = "daily";
    private static final String STRING_WEEKLY_ITEM = "weekly";
    private static final String STRING_MONTHLY_ITEM = "monthly";
    // Constants which indicate activation of missions (ignored - true/activated - false)
    public static final boolean ACTIVATED = false; //Unchecked
    public static final boolean IGNORED = true;    //Checked

    // Provides access to SQLite Database & applying methods.
    UtilityDatabase sqliteDb;

    // Gives access to passed data of Adapter to fill Views with default saved values:
    private Bundle retrievedBundle;
    // Defines which one to use: update or create method.
    private boolean isExisted;

    // TextViews of layout - fields not responsible to clients actions.
    private AppCompatTextView titleType, titleName, titleDescription, titleScore, errorText;
    // Fields responsive to clients actions.
    // EditTexts storing clients considered properties.
    private AppCompatEditText fieldName, fieldScore, fieldDescription;
    // Buttons submitting or canceling creation.
    private AppCompatButton buttonSubmit, buttonCancel, buttonView;
    // Spinner presenting mission types.
    private AppCompatSpinner typeModifierSpinner;
    // Switch View keeps activation profile of mission.
    private SwitchCompat ignoreSwitch;

    // Array<String> Adapts Spinners items.
    private ArrayAdapter<String> spinnerAdapter;

    // Fields storing String, boolean, & numeric data.
    private String missionName, missionDescription;
    // Note: ignore field of new mission considered "false" by default.
    private boolean missionIgnore;
    private int missionType;
    private double missionScore;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = getLayoutInflater().inflate(R.layout.fragment_create_update, container, false);

        // Initializes fields. SGLite Database:
        sqliteDb = new UtilityDatabase(getActivity());
        // TextViews:
        titleType = rootView.findViewById(R.id.type_presenter_txt);
        titleName = rootView.findViewById(R.id.name_presenter_txt);
        titleScore = rootView.findViewById(R.id.score_presenter_txt);
        titleDescription = rootView.findViewById(R.id.description_presenter_txt);
        errorText = rootView.findViewById(R.id.text_input_error);
        // EditTexts:
        fieldName = rootView.findViewById(R.id.name_edit_text);
        fieldScore = rootView.findViewById(R.id.score_edit_text);
        fieldDescription = rootView.findViewById(R.id.description_edit_text);
        // Buttons:
        buttonSubmit = rootView.findViewById(R.id.submit_button);
        buttonCancel = rootView.findViewById(R.id.cancel_button);
        buttonView = rootView.findViewById(R.id.view_button);
        // Spinner & Switch:
        typeModifierSpinner = rootView.findViewById(R.id.type_select_spinner);
        ignoreSwitch = rootView.findViewById(R.id.ignore_switch);

        // Note: Type's field of new mission assigned via spinner view, using its Listener.
        // Note: Ignore field of new mission assigned "false" manually.
        missionIgnore = ACTIVATED;
        // Initializes spinner view by a String Array.
        initTypeSpinner();

        // Fills Views with default data retrieved from existed mission. & Returns true to use update method
        isExisted = assignValueToFieldView();

        // Sets Listener on spinner view to store clients chose type.
        typeModifierSpinner.setOnItemSelectedListener(this);
        // Initializes ignore-activate state of mission, responses to user choose. Set active by default.
        ignoreSwitch.setOnCheckedChangeListener(this::onCheckedChanged);

        // Cancels creation process & backs stack to earlier fragment.
        buttonCancel.setOnClickListener(this::backStack);
        // Creates a new mission & stores it in SQLite database.
        buttonSubmit.setOnClickListener(this::saveMissionRowInDb);
        // Views applied changes in Database & Lists.
        buttonView.setOnClickListener(this::backStack);

        return rootView;
    }

    @Override
    // Refreshes Switch check boolean value to default "false", prevents deploying last config in new mission creation Frag.
    public void onPause() {
        super.onPause();
        missionIgnore = ACTIVATED;
        // Prevents last configuration implication.
        ignoreSwitch.setChecked(ACTIVATED);
    }

    // Stores clients entered data in class-local variables. Returns whether all fields are initialized or not.
    private boolean storeMissionField() {
        // Stores edit texts' values wrote by customer.
        missionName = Objects.requireNonNull(fieldName.getText()).toString().trim();
        missionDescription = Objects.requireNonNull(fieldDescription.getText()).toString().trim();

        // Fills a local temporary variable, checks score field to be filled.
        String tempStringScore = Objects.requireNonNull(fieldScore.getText()).toString().trim();
        if (tempStringScore.isEmpty()) return false;
        else missionScore = Double.parseDouble(tempStringScore);

        // Checks name field to be filled & to be at least 5 char.
        if (missionName.isEmpty() || missionName.length() < 5) return false;
        // Checks type field to be selected.
        return missionType != -1;
        // All fields initialized, therefore returns true.
    }

    // Initializes SpinnerView's items by a String Array.
    private void initTypeSpinner() {
        // An Array String keeps items of type spinner:
        String[] typeItems = new String[]{STRING_DAILY_ITEM, STRING_WEEKLY_ITEM, STRING_MONTHLY_ITEM};
        spinnerAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1, typeItems);
        typeModifierSpinner.setAdapter(spinnerAdapter);
    }

    // Checks all fields to be filled, then takes one of these functions: inserting or updating the mission's row depending on its preexistence.
    private void saveMissionRowInDb(View view) {
        // Assigns received values to local variables.
        boolean isAllInitialized = storeMissionField();
        // Clears showed error text.
        errorText.setVisibility(View.INVISIBLE);

        if (isAllInitialized) {
            // Checks mission to be existed.
            if (isExisted) {
                // Stores name field of mission.
                String newName = missionName;
                // Checks changes in name.
                if (retrievedBundle.getString(DataMission.KEY_NAME).equals(missionName))
                    newName = null;
                // Uses update CRUD operation on SQLite database, alters changes.
                boolean isUpdateUsed = sqliteDb.updateMissionRow(retrievedBundle.getString(DataMission.KEY_NAME), missionType,
                        newName, missionScore, missionDescription, String.valueOf(missionIgnore));
            } else {
                // Creates a new row in SQLite Db using clients filled data.
                sqliteDb.insertMissionRow(missionType, missionName, missionScore, missionDescription, missionIgnore);
                // Provides a success text & a button to view result
                errorText.setText("New mission created.");

                // Frees edit texts to receive new data.
                Objects.requireNonNull(fieldName.getText()).clear();
                Objects.requireNonNull(fieldScore.getText()).clear();
                Objects.requireNonNull(fieldDescription.getText()).clear();
            }
            // Presents view button.
            buttonView.setVisibility(View.VISIBLE);
        } else {
            // Presents an error text asking completing left field(s).
            errorText.setText("Fill out all required fields!");
            buttonView.setVisibility(View.GONE);
        }
        // Provides error or success text.
        errorText.setVisibility(View.VISIBLE);
    }

    // Assigns Views with retrieved Arguments from Bundle of chose mission for update.
    // Note: Returns true if succeed to assign. Else returns false, non using Bundle.
    private boolean assignValueToFieldView() {
        // Initializes Bundle retrieving arguments of populated Bundle in Data Mission Adapter class.
        retrievedBundle = this.getArguments();
        if (retrievedBundle != null) {
            // Sets text "update" to submit button.
            buttonSubmit.setText("update");
            // Sets text of edit text Views.
            fieldName.setText(retrievedBundle.getString(DataMission.KEY_NAME));
            fieldDescription.setText(retrievedBundle.getString(DataMission.KEY_DESCRP));

            // Receives score variable as a Double value, casted to String value, & presents it in score EditText.
            fieldScore.setText(String.valueOf(retrievedBundle.getDouble(DataMission.KEY_SCORE)));

            // Receives int value of field "type".
            int tempIntType = retrievedBundle.getInt(DataMission.KEY_TYPE);
            // Casts int value of field "type" to String.
            String tempStringType = "";
            switch (tempIntType) {
                case DAILY_MISSION:
                    tempStringType = STRING_DAILY_ITEM;
                    break;
                case WEEKLY_MISSION:
                    tempStringType = STRING_WEEKLY_ITEM;
                    break;
                case MONTHLY_MISSION:
                    tempStringType = STRING_MONTHLY_ITEM;
                    break;
            }
            // Retrieves position of int value of field "type".
            int tempIntPosition = spinnerAdapter.getPosition(tempStringType);
            // Assigns retrieved value to spinner, using Position.
            typeModifierSpinner.setSelection(tempIntPosition);

            // Assigns check boolean value to Switch.
            ignoreSwitch.setChecked(retrievedBundle.getBoolean(DataMission.KEY_IGNORE));
            // Stores current "ignore" field of mission.
            missionIgnore = ignoreSwitch.isChecked();

            // Defines to use Bundle arguments or not.
            return retrievedBundle.getBoolean(DataMission.KEY_USE_BUNDLE);
        } // Sets text of button submit to its default.
        else buttonSubmit.setText("submit");

        // Results no Bundle received, don't fill Views by default.
        return false;
    }

    // Saves change in check state of 'ignore' Switch.
    private void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked)      // IsChecked : true, therefore mission ignored.
            missionIgnore = IGNORED;
        else      // IsUnchecked : false, therefore mission activated.
            missionIgnore = ACTIVATED;
    }

    // Stops creation process, then backs stack earlier fragment.
    private void backStack(View view) {
        fragManager.popBackStack();
    }

    @Override
    // Initializes mission type field based on chose spinner's item.
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long itemId) {
        String tempStringType = typeModifierSpinner.getSelectedItem().toString();
        // Casts entered string type to int type.
        switch (tempStringType) {
            case STRING_DAILY_ITEM:
                missionType = DAILY_MISSION;
                break;
            case STRING_WEEKLY_ITEM:
                missionType = WEEKLY_MISSION;
                break;
            case STRING_MONTHLY_ITEM:
                missionType = MONTHLY_MISSION;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // Default chosen item in spinner view is first item, which is "daily".
    }
}
