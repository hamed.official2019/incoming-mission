package ir.hamed.dailymission;

import java.util.ArrayList;

public class DataMission {

    // Public constants indicating keys for database fields(properties of each row)
    public static final String KEY_TYPE = "type";
    public static final String KEY_IGNORE = "ignored";
    public static final String KEY_NAME = "name";
    public static final String KEY_SCORE = "score";
    public static final String KEY_DESCRP = "description";
    // Constant dedicating usage of Bundle & configs of submit-update button in Create-Update Fragment:
    public static final String KEY_USE_BUNDLE = "use";

    // Basic version fields:
    private int type;         // Declared to access easier to the table which takes operations
    private boolean ignored;
    private String name;
    private double score;
    private String description;
    // Premium version fields: right now these fields are just declared.
    private double progressPercentage;
    private ArrayList<String> partName;    // keeps name of progress parts.
    private ArrayList<Double> partScore;   // Keeps specified score of each part.

    // Constructor including all fields in arguments:
    public DataMission(int type, String name, double score, String description, boolean ignored) {
        this.type = type;
        this.name = name;
        this.score = score;
        this.description = description;
        this.ignored = ignored;
    }

    // Non-argumented constructor:
    public DataMission() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Note: score field declared as a double, its getter property returns a String to be able to set on txt view
    public String getScore() {
        return String.valueOf(score);
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
