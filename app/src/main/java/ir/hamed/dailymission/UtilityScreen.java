package ir.hamed.dailymission;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;

public class UtilityScreen {

    private float dpWidth, dpHeight;
    private float pxWidth, pxHeight;
    private float density;

    // This field provided to get access to tools of this class in whole app.
    private Activity activity;

    public UtilityScreen(Activity activity) {
        this.activity = activity;

        // Required utils to get displaying window dimensions in px
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        // Block receiving dimensions & converting px dimensions to dp
        // NOTE: Density is used in calculations which are not mentioned in this (Utility) class.
        this.density = activity.getResources().getDisplayMetrics().density;

        // Dimensions in px (height - width)
        // NOTE: These variables are used in methods aligning share Me txt.
        pxHeight = metrics.heightPixels;
        pxWidth = metrics.widthPixels;

        // Dimensions in dp (height - width)
        // NOTE: These variables may be used in fragments & modifying a layout to be shown
        //       depending on size of displaying screen.
        dpHeight = pxHeight / density;
        dpWidth = pxWidth / density;
    }


    // Getter methods:
    public float getDensity() {
        return density;
    }

    public float getPxWidth() {
        return pxWidth;
    }

    public float getPxHeight() {
        return pxHeight;
    }

    public float getDpWidth() {
        return dpWidth;
    }

    public float getDpHeight() {
        return dpHeight;
    }
}
