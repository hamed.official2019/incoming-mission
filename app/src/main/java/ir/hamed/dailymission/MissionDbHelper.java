package ir.hamed.dailymission;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MissionDbHelper extends SQLiteOpenHelper {

    // Static(final) arguments of constructor method:
    private static final String DB_NAME = "mission_db";
    private static final int DB_VERSION = 1;

    // Database backup directory name,used to get filepath in MainActivity
    private static final String BACKUP_DB_NAME = "backup_db";

    // Constant key-names for tables of mission database
    public static final String TB_DAILY = "daily_mission_tb";
    public static final String TB_WEEKLY = "weekly_mission_tb";
    public static final String TB_MONTHLY = "monthly_mission_tb";

    // Final SQLite commands of creating missions table; used to create, either upgrade tables
    private static final String CMD_CREATE_TB_DAILY = "CREATE TABLE IF NOT EXISTS " + TB_DAILY + " ( " +
            DataMission.KEY_NAME + " TEXT NOT NULL PRIMARY KEY COLLATE NOCASE, " +
            DataMission.KEY_TYPE + " INTEGER NOT NULL, " +
            DataMission.KEY_SCORE + " NUMERIC NOT NULL, " +
            DataMission.KEY_DESCRP + " TEXT, " +
            DataMission.KEY_IGNORE + " BOOLEAN NOT NULL" + ");";

    private static final String CMD_CREATE_TB_WEEKLY = "CREATE TABLE IF NOT EXISTS " + TB_WEEKLY + " ( " +
            DataMission.KEY_NAME + " TEXT NOT NULL PRIMARY KEY COLLATE NOCASE, " +
            DataMission.KEY_TYPE + " INTEGER NOT NULL, " +
            DataMission.KEY_SCORE + " NUMERIC NOT NULL, " +
            DataMission.KEY_DESCRP + " TEXT, " +
            DataMission.KEY_IGNORE + " BOOLEAN NOT NULL" + ");";

    private static final String CMD_CREATE_TB_MONTHLY = "CREATE TABLE IF NOT EXISTS " + TB_MONTHLY + " ( " +
            DataMission.KEY_NAME + " TEXT NOT NULL PRIMARY KEY COLLATE NOCASE, " +
            DataMission.KEY_TYPE + " INTEGER NOT NULL, " +
            DataMission.KEY_SCORE + " NUMERIC NOT NULL, " +
            DataMission.KEY_DESCRP + " TEXT, " +
            DataMission.KEY_IGNORE + " BOOLEAN NOT NULL" + ");";

    // Drops tables of database to set updates
    private static final String CMD_DROP_DAILY_TB = "DROP TABLE IF EXISTS " + TB_DAILY + ";";
    private static final String CMD_DROP_WEEKLY_TB = "DROP TABLE IF EXISTS " + TB_WEEKLY + ";";
    private static final String CMD_DROP_MONTHLY_TB = "DROP TABLE IF EXISTS " + TB_MONTHLY + ";";


    public MissionDbHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CMD_CREATE_TB_DAILY); // To create day table if not existed
        sqLiteDatabase.execSQL(CMD_CREATE_TB_WEEKLY); // To create week table if not existed
        sqLiteDatabase.execSQL(CMD_CREATE_TB_MONTHLY); // To create month table if not existed
    }

    @Override
    // Deletes existed Database & only creates Tables & sets their properties. Todo: Backup data is in high DEMAND
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // Drops current database
        sqLiteDatabase.execSQL(CMD_DROP_DAILY_TB);
        sqLiteDatabase.execSQL(CMD_DROP_WEEKLY_TB);
        sqLiteDatabase.execSQL(CMD_DROP_MONTHLY_TB);
        // Creates new database & sets Db version to 1 automatically
        onCreate(sqLiteDatabase);
        // TODO: set a Query to restore backup data to the new database
    }

}
