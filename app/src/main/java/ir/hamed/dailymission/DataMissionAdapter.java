package ir.hamed.dailymission;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class DataMissionAdapter extends RecyclerView.Adapter<DataMissionAdapter.DataMissionViewHolder> {

    // Instantiate of data class declared as a field of this adapter class - the field is an array list
    private List<DataMission> dataMissionList;

    public DataMissionAdapter(List<DataMission> dataMissionList) {
        this.dataMissionList = (dataMissionList != null) ? dataMissionList : new ArrayList<>();
    }

    @NonNull
    @Override
    // inflates the row layout from xml when needed
    public DataMissionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.each_misson_display, parent, false);
        return new DataMissionViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull DataMissionViewHolder holder, int position) {
        holder.bind(this.dataMissionList.get(position));

        // Instantiates an anime & sets its source & duration.
        Animation populateRowAnime = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.populate_row);
        // Sets anime on rows on binding.
        holder.itemView.setAnimation(populateRowAnime);

        // Sets visibility of Recyclers divider invisible if there is no data in mission's List.
        if (holder.getAdapterPosition() == (this.dataMissionList.size() - 1))
            holder.divider.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return this.dataMissionList.size();
    }

    @Override
    // Modifies which List(ViewType) should return.
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class DataMissionViewHolder extends RecyclerView.ViewHolder {

        // Constants which indicate activation of missions (ignored - true/activated - false)
        public static final String ACTIVATED = "false"; //Unchecked
        public static final String IGNORED = "true";    //Checked
        // Constant which indicates no change in type while updating a row's data in Database:
        public static final int NO_TYPE_CHANGE = 0;

        // An instantiate of database open helper to access to SQLite database & to take operation:
        private UtilityDatabase sqliteDb;
        // Selected row's data stored in temporary variables:
        private String tempStringName, tempStrDescription;
        private int tempIntType;
        private Double tempDoubleScore;
        private boolean tempBoolIgnore;

        // NOTE: Premium version fields are not observed!
        // Basic version fields:
        private AppCompatTextView missionName, missionScore, missionDescription;
        private final AppCompatTextView scoreTitle, descriptionTitle;
        private View divider;
        // Buttons or SwitchView receiving Listeners:
        private SwitchCompat ignoreSwitch;
        private AppCompatImageButton buttonEdit, buttonDelete;

        DataMissionViewHolder(@NonNull View itemView) {
            super(itemView);
            // Accesses SQLite database.
            sqliteDb = new UtilityDatabase(itemView.getContext());
            // Each row Views:
            missionName = itemView.findViewById(R.id.mission_name);
            missionScore = itemView.findViewById(R.id.mission_score);
            missionDescription = itemView.findViewById(R.id.mission_description);
            // These text fields assigned as default - presented to keep design alignments
            scoreTitle = itemView.findViewById(R.id.score_title);
            descriptionTitle = itemView.findViewById(R.id.description_title);
            divider = itemView.findViewById(R.id.each_mission_divider);
            // Buttons or SwitchView:
            ignoreSwitch = itemView.findViewById(R.id.ignore_switch);
            buttonDelete = itemView.findViewById(R.id.delete_button);
            buttonEdit = itemView.findViewById(R.id.edit_button);
        }

        // Util bind method used in adapter class
        private void bind(DataMission dataMission) {
            missionName.setText(dataMission.getName());
            scoreTitle.setText("score : ");
            missionScore.setText(dataMission.getScore());
            missionDescription.setText(dataMission.getDescription());
            // Removes description & its title if no description stored in.
            if (missionDescription.length() == 0) {
                missionDescription.setVisibility(View.GONE);
                descriptionTitle.setVisibility(View.GONE);
            }
            // Presents "Description:" title in description inserted rows if any description stored in.
            else {
                descriptionTitle.setVisibility(View.VISIBLE);
                descriptionTitle.setText("Description:");
            }
            ignoreSwitch.setChecked(dataMission.isIgnored());

            // Temporary variables keeping selected:
            tempStringName = dataMission.getName();
            tempStrDescription = dataMission.getDescription();
            tempDoubleScore = Double.valueOf(dataMission.getScore());
            tempIntType = dataMission.getType();
            tempBoolIgnore = dataMission.isIgnored();

            // Saves change in ignore state to a specific row.
            ignoreSwitch.setOnCheckedChangeListener(this::onCheckedChanged);
            // Deletes selected item.
            buttonDelete.setOnClickListener(this::deleteRow);
            // Updates selected mission's fields.
            buttonEdit.setOnClickListener(this::callUpdateFrag);
        }

        // Registers a callback to be invoked when the checked state of ignore switch changes.
        // True to check the button, false to uncheck it. Thanks to google Android dev. doc.
        private void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            // Update method of SQLite Db helper's ignore variable declared String, Stores temp String value:
            String tempStringIgnore;
            if (isChecked) { // IsChecked : true, therefore mission ignored.
                // Stores true text on temp variable.
                tempStringIgnore = IGNORED;
                // Toasts event success, ignoring.
                Toast.makeText(compoundButton.getContext(),
                        "Mission ignored.Later you can activate it or delete it permanently",
                        Toast.LENGTH_SHORT).show();
            } else { // IsUnchecked : false, therefore mission activated.
                // Stores false text on temp variable.
                tempStringIgnore = ACTIVATED;
                // Toasts event success, activating.
                Toast.makeText(compoundButton.getContext(), "Mission activated.",
                        Toast.LENGTH_SHORT).show();
            }

            // Alters change in ignore state to the specific row,
            // all unchanged fields assigned key-values defining no change in current value including: null , -0.00 , -0.
            sqliteDb.updateMissionRow(tempStringName, NO_TYPE_CHANGE, null, 0.00, null, tempStringIgnore);
        }

        // Deletes selected item.
        @SuppressLint("NotifyDataSetChanged")
        private void deleteRow(View selectedRow) {
            sqliteDb.deleteMissionRow(tempStringName);
            dataMissionList.remove(selectedRow.getVerticalScrollbarPosition());
            notifyDataSetChanged();
        }

        // Puts selected mission's data in a Bundle to transit to Update-create Fragment.
        private void callUpdateFrag(View view) {
            // Puts ContentValues of existed row in a Bundle to pass to another fragment.
            Bundle producedBundle = new Bundle();
            // Determines the usage of new Bundle in creation of 'Create-Update' Fragment.
            producedBundle.putBoolean(DataMission.KEY_USE_BUNDLE, true);
            producedBundle.putString(DataMission.KEY_NAME, tempStringName);
            producedBundle.putString(DataMission.KEY_DESCRP, tempStrDescription);
            producedBundle.putDouble(DataMission.KEY_SCORE, tempDoubleScore);
            producedBundle.putInt(DataMission.KEY_TYPE, tempIntType);
            producedBundle.putBoolean(DataMission.KEY_IGNORE, tempBoolIgnore);

            // Passes data to new Create-Update Fragment.
            FragmentCreateUpdate updateMissionFrag = new FragmentCreateUpdate();
            // Assigns Bundle arguments.
            updateMissionFrag.setArguments(producedBundle);

            // Defines which fragment Layout to be replaced by frag Update Mission.
            int currentFragId;
            // Note: the boolean isTwoPane variable is in MainActivity initialized.
            if (MainActivity.isTwoPane)  // Result is true - A wide display screen device is running the App
                currentFragId = R.id.fragment_mission_container;
            else                         // Result is false - A small display screen device is running the App
                currentFragId = R.id.fragment_main_scores_container;

            // Begins Transition using assigned Bundle to create new Frag.
            MainActivity.fragManager.beginTransaction()
                    .setCustomAnimations(
                            R.anim.right_slide_in_frag,         //enter anime
                            R.anim.fade_out_frag,               //exit anime
                            R.anim.fade_in_frag,                //popEnter anime
                            R.anim.right_slide_out_frag         //popExit anime
                    ).replace(currentFragId, updateMissionFrag)
                    .setReorderingAllowed(true)
                    .addToBackStack("update")
                    .commit();
        }
    }
}
